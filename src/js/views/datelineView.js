import {elements} from './base'
import Moment from 'moment';
import { extendMoment } from 'moment-range';
const moment = extendMoment(Moment);


export const clearReturnBtn = () => {
  document.querySelector('.return-btn').style.display = 'none'
}


export const renderReturnBtn = () => {
  document.querySelector('.return-btn').style.display = "block"

}

export const clearDateline = () => {
  elements.dateline.innerHTML = ''
}

export const formatDay = (date) => {
  const options = { weekday: 'short'};
  return date.toLocaleDateString("en-US", options).toUpperCase().charAt(0);
}

export const formatMonth = (date) => {
  const options = { year: "numeric", month: "long"};
  return date.toLocaleDateString("en-US", options)
}

export const highlightToday = (todayIndex) => {
  if (todayIndex.length > 0){
    document.getElementById("date-" + todayIndex).classList.add("today-date")
  }
}

export const renderAvailability = (availabilityIndex, todayIndex) => {
  availabilityIndex.forEach((el, index) =>{
    if (el >= todayIndex){
      document.getElementById(`date-${el}`).classList.add("available")
      document.getElementById(`date-${el}`).classList.remove('no-highlight')
      document.getElementById(`date-${el}`).classList.add("highlight")
    }
  })
}

export const renderDateline = dateline => {
  const markup = `
        <div class="dateline-container">
            <label class="month-label">${formatMonth(dateline.month)}</label>
            <ul class="dateline">
                  <li class="date-item-2">
                      <div class="day">${formatDay(dateline.dateRange[0])}</div>
                      <div id="date-0" class="date no-highlight">${dateline.dateRange[0].getDate()}</div>
                  </li>
                  <li class="date-item-3">
                      <div class="day">${formatDay(dateline.dateRange[1])}</div>
                      <div id="date-1" class="date no-highlight">${dateline.dateRange[1].getDate()}</div>
                  </li>
                  <li class="date-item-4">
                      <div class="day">${formatDay(dateline.dateRange[2])}</div>
                      <div id="date-2" class="date no-highlight">${dateline.dateRange[2].getDate()}</div>
                  </li>
                  <li class="date-item-5 ">
                      <div class="day">${formatDay(dateline.dateRange[3])}</div>
                      <div id="date-3" class="date no-highlight">${dateline.dateRange[3].getDate()}</div>
                  </li>
                  <li class="date-item-6 ">
                      <div class="day">${formatDay(dateline.dateRange[4])}</div>
                      <div id="date-4" class="date no-highlight">${dateline.dateRange[4].getDate()}</div>
                  </li>
                  <li class="date-item-7 ">
                      <div class="day">${formatDay(dateline.dateRange[5])}</div>
                      <div id="date-5" class="date no-highlight">${dateline.dateRange[5].getDate()}</div>
                  </li>
                  <li class="date-item-8 ">
                      <div class="day">${formatDay(dateline.dateRange[6])}</div>
                      <div id="date-6" class="date no-highlight">${dateline.dateRange[6].getDate()}</div>
                  </li>
                  <li class="date-item-9 ">
                      <div id="date-7" class="date no-highlight">${dateline.dateRange[7].getDate()}</div>
                  </li>
                  <li class="date-item-10 ">
                      <div id="date-8" class="date no-highlight">${dateline.dateRange[8].getDate()}</div>
                  </li>
                  <li class="date-item-11 ">
                      <div id="date-9" class="date no-highlight">${dateline.dateRange[9].getDate()}</div>
                  </li>
                  <li class="date-item-12 ">
                      <div id="date-10" class="date no-highlight">${dateline.dateRange[10].getDate()}</div>
                  </li>
                  <li class="date-item-13 ">
                      <div id="date-11" class="date no-highlight">${dateline.dateRange[11].getDate()}</div>
                  </li>
                  <li class="date-item-14 ">
                      <div id="date-12" class="date no-highlight">${dateline.dateRange[12].getDate()}</div>
                  </li>
                  <li class="date-item-15 ">
                      <div id="date-13" class="date no-highlight">${dateline.dateRange[13].getDate()}</div>
                  </li>
              </ul>
          </div>
      `
  elements.dateline.insertAdjacentHTML('beforeend', markup)
}

export const renderMoreBtn = () => {
  const markup = `
        <div class="more-times-container">
          <span class="more-times-text">Open houses</span>
          <img class="chevron-icon-right" src="img/chevron-right-blue.svg" alt="">
        </div>
  `
  elements.dateline.insertAdjacentHTML('beforeend', markup)
}




export const scrollTop = () => {
  (document.documentElement || document.body.parentNode || document.body).scrollTop = 0;
}
