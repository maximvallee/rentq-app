import {elements} from './base'
import moment from 'moment'

export const clearTimeline = () => {
  elements.timeline.innerHTML = ''
}

export const formatDay = (date) => {
  const options = { weekday: 'short', month: 'short', day: 'numeric'};
  return date.toLocaleDateString("en-US", options)
}

export const renderTimeline = (selectedDay, intervals) => {
  const markup = `
        <ul class="content-container timeline">
            <label class="date-label">${moment(selectedDay).format('ddd, MMM D')}</label>
            <div class="scrollbox">
                ${intervals.map((el, index) => createTimeCell(el, index)).join(' ')}
            </div>
  `
  elements.timeline.insertAdjacentHTML('beforeend', markup)
}

const createTimeCell = (el, index) => `
    <li id="${index}" class="cell">
    <div id="time-select-${index}" class="time-select-container">
      <span class="time-text" id="time-text-${index}">${moment(el).format('hh:mm a')}</span>
    </div>
    <div id="time-validate-${index}" class="time-validate-container">
      <div class="time-confirm" id="time-confirm-${index}">
        <span id="time-confirm-label-${index}">Confirm</span>
      </div>
      <div class="time-cancel" id="time-cancel-${index}">
        <span id="time-cancel-label-${index}">Nevermind</span>
      </div>
    </div>
  </li>
  `
  

export const renderConfirmBtn = (id) => {
  document.getElementById(`time-validate-${id}`).style.display = "flex";
  document.getElementById(`time-text-${id}`).textContent += " ?"
  document.getElementById(`time-select-${id}`).classList.add("selected")
  document.getElementById(`time-select-${id}`).style.cursor = "auto"
  document.getElementById(`time-select-${id}`).scrollIntoView({block: "center"})
}

export const clearConfirmBtn = (id) => {
  document.getElementById(`time-validate-${id}`).style.display = "none";
  document.getElementById(`time-select-${id}`).classList.remove("selected")
  document.getElementById(`time-select-${id}`).style.cursor = "pointer"

  const timeLabel = (document.getElementById(`time-text-${id}`).textContent)
  document.getElementById(`time-text-${id}`).textContent = timeLabel.replace(' ?', '');
}

export const scrollToElement = (id) => {
  document.getElementById(`time-select-${id}`).scrollIntoView({block: "center"})
}

