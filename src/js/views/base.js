export const elements = {
    heading: document.querySelector('.heading'),
    header: document.querySelector('.heading-section'),
    
    landing: document.querySelector('.landing-page'),
    dateline: document.querySelector('.dateline-page'),
    timeline: document.querySelector('.timeline-page'),
    userform: document.querySelector('.userform-page'),
    disclaimer: document.querySelector('.disclaimer-page'),
    confirmation: document.querySelector('.confirmation-page'),
    footer: document.querySelector('.footer')
}

export const progress = {
  level0: 20, // DateQueue
  level1: 20, // Dateline
  level2: 40, // Timeline, TimeQueue
  level3: 60, // Useform
  level4: 90 // Disclaimer
}

