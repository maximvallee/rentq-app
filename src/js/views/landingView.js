import {elements} from './base'
import moment from 'moment'

export const clearHeading = () => {
  elements.header.innerHTML = ''
}

export const updateProgress = (level) => {
  document.querySelector('.progress-blue').style.width = `${level}%`
  document.querySelector('.progress-label').textContent = `${level}%`
}

export const clearLanding = () => {
  elements.landing.innerHTML = ''
}

export const formatDay = (date) => {
  const options = { weekday: 'short', month: 'short', day: 'numeric'};
  return date.toLocaleDateString("en-US", options)
}

export const renderLanding = (appointments) => {
  const markup = `
              <div class="content-container queue">
                    <div class="queue-text">
                        <p>Below is a list of already scheduled open-houses for this property. Select a time that suits you, or schedule a private viewing.</p>
                    </div>
                    <div class="scrollbox small">
                        ${appointments.map((el, index) => createQueueCell(el, index)).join(' ')}
                    </div>
                    <div class="more-times-container">
                      <span class="more-times-text">Private viewing</span>
                      <img class="chevron-icon-right" src="img/chevron-right-blue.svg" alt="">
                    </div>
              </div>
  `
  elements.landing.insertAdjacentHTML('beforeend', markup)
}

const createQueueCell = (el, index) => `
  <div class="select-container-lg" id="${index}">
  <div class="select-item" id="select-item-${index}">
      <div class="date-queue" id="date-queue-${index}">${moment(el['start_time']).format('ddd, MMM D')}</div>
      <div class="time-queue" id="time-queue-${index}">${moment(el['start_time']).format('h:mm a')} - ${moment(el['end_time']).format('h:mm a')}</div>
  </div>
  <div>
      <img id="chevron-gray-sm-${index}" src="img/chevron-right-grey.svg" alt="">
  </div>
</div>
  `

